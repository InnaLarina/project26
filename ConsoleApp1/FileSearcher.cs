﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class FileSearcher
    {
        public event EventHandler<FileFoundArgs> FileFound;
        public delegate void MethodContainer(string directory);
        public event MethodContainer onNotFoundDirectory;

        public void Search(string directory, string searchPattern)
        {
            int cnt = 1;
            if (!Directory.Exists(directory)) 
            { 
                onNotFoundDirectory(directory);
                return;
            }
            foreach (var file in Directory.EnumerateFiles(directory, searchPattern))
            {
                ++cnt;
                var args = new FileFoundArgs(file, cnt);
                FileFound?.Invoke(this, args);
                if (args.CancelRequested)
                    break;
                
            }
        }
        
    }
    
}
