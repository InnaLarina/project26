﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ConsoleApp1
{
    static class Program
    {

        const int limit = 5;
        static void Main(string[] args)
        {
            Console.WriteLine("Максимальный элемент списка: ");
            List<string> listStr = new List<string> { ("мама"), ("мыла"), ("панораму") };
            Console.WriteLine("{0}", String.Join("; ", listStr));
            String maxString = Extensions.GetMax<string>(listStr, x => (x == null) ? 0 : x.Length);
            Console.WriteLine($"Это слово: {maxString}");
            Console.WriteLine();
            if (args.Length == 0)
            {
                System.Console.WriteLine("Не ввели директорию поиска файла как аргумент командной строки.");
                Console.ReadKey();
                return;
            }
            if (!String.IsNullOrEmpty(args[0])) 
            {
                
                Console.WriteLine($"Список файлов в каталоге. Показываем первых {limit} записей.");
                FileSearcher fs = new FileSearcher();
                fs.onNotFoundDirectory += MessageOnDirectoryNotExist;
                fs.FileFound += onFileFound;
                fs.Search(args[0], "*.*");
                Console.ReadKey();
            }
        }
        
        static void MessageOnDirectoryNotExist(string directory) 
        {
            Console.WriteLine($"Не существует директории {directory}.");
        }

        static public EventHandler<FileFoundArgs> onFileFound = (sender, eventArgs) =>
        {
            Console.WriteLine(eventArgs.FoundFile);
            if(eventArgs.CountFile> limit) eventArgs.CancelRequested = true;
        };
    }

}
