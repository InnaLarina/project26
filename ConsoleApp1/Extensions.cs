﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    static class Extensions
    {
        public static T GetMax<T>(this IEnumerable<T> e, Func<T, float> getParametr) where T : class
        {
            T max = default(T);
            foreach (T item in e)
            {
                float m1 = getParametr(item);
                float m2 = getParametr(max);
                if (getParametr(item) > getParametr(max)) max = item;
            }
            return max;
        }
    }
}
