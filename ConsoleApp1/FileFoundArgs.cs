﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class FileFoundArgs : EventArgs
    {
        public string FoundFile { get; }
        public bool CancelRequested { get; set; }
        public int CountFile { get; }
        public FileFoundArgs(string fileName, int countFiles)
        {
            FoundFile = fileName;
            CountFile = countFiles;
        }
    }
}
